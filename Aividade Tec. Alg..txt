Questão 1 

#include <stdio.h>

// Samuel Luciano Dos Santos Souza
// ADS P.2 NOITE

int main()
{
  
    char numero_original[1];
    char numero_copiado[1] = {'b'};
    int i = 0;

    printf("Digite o valor: ");
    scanf("%c", &numero_original[i]);

    numero_copiado[i] = numero_original[i];

    printf("o valor copiado é igual a: %c",numero_copiado[i]);

    return 0;

}

Questão 2

#include <stdio.h>

//Samuel Luciano Dos Santos Souza
//ADS P.2 NOITE

int main() {
    int i = 0;
    char nome1[15], nome2[15];
    printf("Digite um valor: ");
    scanf("%s",nome1);
    printf("Digite outro valor: ");
    scanf("%s", nome2);
    while (nome1[i] ==  nome2[i]) {
        if (nome1[i] == '\0' || nome2[i] == '\0') {
        break;
        }
    i++;
    }
    if (nome1[i] == '\0' && nome2[i] == '\0') {
      printf("Os valores são iguais.");
      return 0;
    } else {
      printf("Os valores são diferentes.");
      return -1;
    }

}
Questão 3

#include <stdio.h>

//Samuel Luciano Dos Santos Souza 
//ADS P.2 NOITE
int main()
{
    char valor1[15]; 
    char valor2[15]; 
   
     
    printf("Digite um valor: ");
    scanf("%s", valor1);
    printf("Digite um segundo valor: ");
    scanf("%s", valor2);

    printf ("\nEstou concatenando %s com o valor  %s",valor1, valor2);
  return 0; 
}

Questão 4

#include<stdio.h>

//Samuel Luciano Dos Santos Souza
//ADS P.2 NOITE

int main(){
    int contando(char *str);
    char valor[200];
    int qtd_caracters;

    printf("Digite uma frase: ");
    gets(valor);
    
    qtd_caracters = contando(valor);    
    printf("a quantidade de letras é: %d", qtd_caracters);

    
    return 0;
  
}
int contando(char * palavra)
{    
    int i=0;
    while( palavra[i] != '\0')
        i++;
    return i;
  }

Questão 5

#include <stdio.h>

//Samuel Luciano Dos Santos Souza 
// ADS P.2 NOITE

void main() {
char palavra[41],contrario[41];
int total=0,i,j,erro=0;

printf("Digite uma palavra: ");
scanf("%s",palavra);

total = strlen(palavra);
j = total;
total++;
j--;

for(i=0; i < total; i++) {
   contrario[i] = palavra[j];
   j--;
}

total--;

for(i=0; i < total; i++) {
   if(palavra[i] != contrario[i]) { erro = 1; }
}

if(erro == 1) printf("\n a palavra é um palindromo.");
else printf("\na palavra não é palindromo.");
}
